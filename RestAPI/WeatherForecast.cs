using System;

namespace RestAPI
{
    /// <summary>
    /// Model to be used.
    /// </summary>
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public string Summary { get; set; }
    }
}
