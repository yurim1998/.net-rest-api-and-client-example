﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestClient.Model
{
    /// <summary>
    /// WheatherForecast model, same as used by the API.
    /// </summary>
    public class WeatherForecast
    {
        public DateTime date { get; set; }
        public int temperatureC { get; set; }
        public string summary { get; set; }
    }
}
