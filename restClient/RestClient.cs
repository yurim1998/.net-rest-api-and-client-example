using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace RestClient
{
    public enum HttpMethod
    {
        GET,
        POST,
        PUT,
        DELETE,
    }

    public class RestClient
    {
        public string EndPoint { get; set; }
        public HttpMethod HttpMethod { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public RestClient()
        {
            EndPoint = string.Empty;
            HttpMethod = HttpMethod.GET;
        }

        /// <summary>
        /// Sends a GET request to the Http EndPoint.
        /// </summary>
        /// <param name="isSuccessful"></param>
        /// <returns>Json in a successful connection.</returns>
        public string MakeRequest(out bool isSuccessful)
        {
            string responseValue = string.Empty;
            isSuccessful = false;

            try
            {
                // Requisition.
                HttpWebRequest request = WebRequest.Create(EndPoint) as HttpWebRequest;
                request.Method = HttpMethod.ToString();

                // Response. The "using" method will make sure this will only exists inside this context.
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new ApplicationException("Erro: " + response.StatusCode.ToString());
                    }

                    // Process the response
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            isSuccessful = true;
                            using (StreamReader reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // In case of an exception, returns an error message instead of crashing.
                responseValue = e.Message;
            }

            return responseValue;
        }
    }
}
