﻿namespace RestClient
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxURI = new System.Windows.Forms.TextBox();
            this.TextBoxResponse = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelResponse = new System.Windows.Forms.Label();
            this.ButtonRequest = new System.Windows.Forms.Button();
            this.DataGridWeather = new System.Windows.Forms.DataGridView();
            this.Datetime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Temperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Summary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeather)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxURI
            // 
            this.TextBoxURI.Location = new System.Drawing.Point(91, 13);
            this.TextBoxURI.Name = "TextBoxURI";
            this.TextBoxURI.Size = new System.Drawing.Size(371, 23);
            this.TextBoxURI.TabIndex = 0;
            // 
            // TextBoxResponse
            // 
            this.TextBoxResponse.Location = new System.Drawing.Point(91, 42);
            this.TextBoxResponse.Multiline = true;
            this.TextBoxResponse.Name = "TextBoxResponse";
            this.TextBoxResponse.Size = new System.Drawing.Size(498, 174);
            this.TextBoxResponse.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Request URI";
            // 
            // LabelResponse
            // 
            this.LabelResponse.AutoSize = true;
            this.LabelResponse.Location = new System.Drawing.Point(25, 45);
            this.LabelResponse.Name = "LabelResponse";
            this.LabelResponse.Size = new System.Drawing.Size(60, 15);
            this.LabelResponse.TabIndex = 3;
            this.LabelResponse.Text = "Response:";
            // 
            // ButtonRequest
            // 
            this.ButtonRequest.Location = new System.Drawing.Point(469, 13);
            this.ButtonRequest.Name = "ButtonRequest";
            this.ButtonRequest.Size = new System.Drawing.Size(120, 23);
            this.ButtonRequest.TabIndex = 4;
            this.ButtonRequest.Text = "Request";
            this.ButtonRequest.UseVisualStyleBackColor = true;
            this.ButtonRequest.Click += new System.EventHandler(this.ButtonRequest_Click);
            // 
            // DataGridWeather
            // 
            this.DataGridWeather.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridWeather.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Datetime,
            this.Temperature,
            this.Summary});
            this.DataGridWeather.Location = new System.Drawing.Point(91, 222);
            this.DataGridWeather.Name = "DataGridWeather";
            this.DataGridWeather.ReadOnly = true;
            this.DataGridWeather.RowHeadersVisible = false;
            this.DataGridWeather.RowTemplate.Height = 25;
            this.DataGridWeather.Size = new System.Drawing.Size(498, 187);
            this.DataGridWeather.TabIndex = 5;
            // 
            // Datetime
            // 
            this.Datetime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Datetime.HeaderText = "Data";
            this.Datetime.Name = "Datetime";
            this.Datetime.ReadOnly = true;
            // 
            // Temperature
            // 
            this.Temperature.HeaderText = "Temperatura";
            this.Temperature.Name = "Temperature";
            this.Temperature.ReadOnly = true;
            // 
            // Summary
            // 
            this.Summary.HeaderText = "Sumário";
            this.Summary.Name = "Summary";
            this.Summary.ReadOnly = true;
            this.Summary.Width = 200;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 421);
            this.Controls.Add(this.DataGridWeather);
            this.Controls.Add(this.ButtonRequest);
            this.Controls.Add(this.LabelResponse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxResponse);
            this.Controls.Add(this.TextBoxURI);
            this.Name = "FormMain";
            this.Text = "Weather Requester";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeather)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxURI;
        private System.Windows.Forms.TextBox TextBoxResponse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabelResponse;
        private System.Windows.Forms.Button ButtonRequest;
        private System.Windows.Forms.DataGridView DataGridWeather;
        private System.Windows.Forms.DataGridViewTextBoxColumn Datetime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Temperature;
        private System.Windows.Forms.DataGridViewTextBoxColumn Summary;
    }
}

