﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Newtonsoft.Json;
using RestClient.Model;

namespace RestClient
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            DataGridWeather.Rows.Clear();
        }

        /// <summary>
        /// Event triggered when the button is clicked.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event.</param>
        private void ButtonRequest_Click(object sender, EventArgs e)
        {
            // Instances the Client object.
            RestClient restClient = new RestClient();
            restClient.EndPoint = TextBoxURI.Text;

            // Makes a Http request.
            string strResponse = string.Empty;
            bool isSuccessful;
            strResponse = restClient.MakeRequest(out isSuccessful);
            TextBoxResponse.Text = strResponse;

            // If got something, converts it to something easy to read.
            if (isSuccessful)
                ConvertJson(strResponse);
        }

        /// <summary>
        /// Deserialize the json into a more readable format and display it in a DataGrid.
        /// </summary>
        /// <param name="json">Json.</param>
        private void ConvertJson(string json)
        {
            List<WeatherForecast> list = JsonConvert.DeserializeObject<List<WeatherForecast>>(json);
            foreach (WeatherForecast forecast in list)
            {
                DataGridWeather.Rows.Add();
                DataGridWeather.Rows[DataGridWeather.Rows.Count - 2].Cells[0].Value = forecast.date;
                DataGridWeather.Rows[DataGridWeather.Rows.Count - 2].Cells[1].Value = forecast.temperatureC.ToString()+"ºC";
                DataGridWeather.Rows[DataGridWeather.Rows.Count - 2].Cells[2].Value = forecast.summary;
            }
        }
    }
}
